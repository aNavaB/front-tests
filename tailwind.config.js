module.exports = {
  purge: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  mode: 'jit',
  theme: {
    extend: {
      padding:{
        '10%': '10%',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
